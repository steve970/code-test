# Code Challenges
This is a test in which candidates will try to complete as many challenges as possible in 2 hours. There is no particular order and you may start or finish them in any order you choose. These challenges are simply meant to gauge your level of knowledge in certain aspects web development. Do your best to provide the most efficient and smart solution to each problem. Upon completion, please be prepared to review your code with us and explain your solutions. Also if you are unable to solve any part of a challenge, please feel free to ask questions after the test is complete. Thanks!

## To start
Clone the repo to your local machine to begin.

If you wish to start a server you can run the code: `$ php -S localhost:8000`

## Challenge 1
**Address Book Challenge:**  
The goal of this challenge is to parse and format a string into a JS object in which the data can be used later.  
- [view source](./address-select/index.html)


## Challenge 2
**Responsive Logo Challenge:**  
The goal of this challenge is to position an image on the screen using responsive CSS.  
- [view source](./responsive-logo/index.html)


## Challenge 3
**Responsive Text Challenge:**  
The goal of this challenge is to add proper HTML tags and manipulate elements using responsive CSS.
- [view source](./responsive-text/index.html)


## Challenge 4
**Reversal Challenge:**  
The goal of this challenge is to take a word like Desserts reverse it to Stressed and outout them togeter.
- [view source](./reversal/index.html)


## Challenge 5
**Senator Challenge:**  
The goal of this challenge is to use ajax and find a senator by state through a text input field.
- [view source](./senator-finder/index.html)


## Bonus Challenge 6
**Nearest Numerical Palindrome Challenge:**  
The goal of this challenge is to take the number 2345. Using JS find the closet palindrome either below or above by count.
- [view source](./number-palindrome/index.html)



